window.onload = createFormElements;
function createFormElements() {
    generateTable();
    document.getElementById("row-count").addEventListener("change", generateTable);
    document.getElementById("col-count").addEventListener("change", generateTable);
    document.getElementById("text-color").addEventListener("change", generateTable);
    document.getElementById("background-color").addEventListener("change", generateTable);
    document.getElementById("table-width").addEventListener("change", generateTable);
    document.getElementById("border-color").addEventListener("change", generateTable);
}

function generateTable() {
    //targetting body of where the table is going to be
    let body = document.getElementById("table-render-space");
    body.innerHTML = "";
    //getting how many rows the user input
    let row = document.getElementById("row-count").value;
    //getting how many columns the user input
    let column = document.getElementById("col-count").value;
    //creating a table element
    let table = document.createElement("table");
    //for loop that creats each row depending on what the user put as rows
    for (let i = 0; i < row; i++ ){
        //creating a row
        let rowCreate = document.createElement("tr");
        //for loop that creates a cell and puts the text "cell" in it
        for (let j = 0; j < column; j++) {
            let cellCreate = document.createElement("td");
            let cellText = document.createTextNode("cell" + i + j);
            //putting the text cell into the cell
            cellCreate.appendChild(cellText);
            //putting the cell inside the row
            rowCreate.appendChild(cellCreate);
        }
        //adding the row to the table
        table.appendChild(rowCreate);
    }
    //adding the table to the table-render-space area
    body.appendChild(table);
    let textColor = document.getElementById('text-color').value;
    let tableWidth = document.getElementById('table-width').value;
    let backColor = document.getElementById('background-color').value;
    let bordColor = document.getElementById('border-color').value;
    let cell = document.querySelectorAll('td');
    changeTagWidth(table, tableWidth);
    changeTagTextColor(table, textColor);
    changeTagBackground(table, backColor);
    changeTagBorderColor(cell, bordColor);
    
}