function changeTagTextColor(tag, color){
    tag.style.color = `${color}`;
}

function changeTagBackground(tag, color) {
    tag.style.backgroundColor = `${color}`;
}

function changeTagWidth(tag, width){
    tag.style.width = `${width}%`;
}

function changeTagBorderColor(tag, color) {
    for(element in tag){
        element.style.borderColor = color;
    }
}